<?php
/*
    Template Name: Rubriques Template
*/
get_header();
the_post();
?>
    <section class="header not-full-screen" style="background-image: url( <?= get_template_directory_uri() . '/imgs/backgrounds/back-header-home.png' ?>);">
        <nav class="main-nav row">
            <div class="col-3">
                <div class="logo-container">
                    <img src=<?= get_template_directory_uri() . "/imgs/logo/logo-blanc.png" ?> class="main-logo"/>
                </div>
            </div>
            <div class="col-6">
                <ul class="home-main-menu">
                    <li><a href="<?=  site_url()?>">Accueil</a></li>
                    <li><a href="<?=  site_url()?>/a-propos">A propos</a></li>
                    <li><a href="<?=  site_url()?>/rubriques/">Rubriques</a></li>
                    <li><a href="<?=  site_url()?>/experts/">Experts</a></li>
                </ul>
            </div>
            <div class="col-3 row">
                <div class="col-9 social-menu px-0">
                    <ul>
                        <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/search.svg" ?> class="search-icon" /></a></li>
                        <li><span class="split-icon"></span></li>
                        <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/youtube.svg" ?> class="search-icon" /></a></li>
                        <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter.svg" ?> class="search-icon" /></a></li>
                        <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook.svg"?> class="facebook-icon" /></a></li>
                    </ul>
                </div>
                <div class="col-3 language-menu">
                    <ul>
                        <li class="active"><a href="#"/>Fr </a></li>
                        <li>               <a href="#"/>Eng</a></li>
                        <li class="ar">    <a href="#"/>ع</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="page-header  offset-md-2 col-md-8 pb-5">
            <div class="header-title py-5 ">
                <h1 class="white text-center fs-big mb-0"><?= get_the_title()?></h1>
            </div>
        </div>
    </section>

    <div class="rebric-theme" style="background-color: #9b8159"></div>

    <section>
        <div class="container-fluid py-5">
            <div class="rebrics-container pt-5 ">
                <div class="rebric-card securite-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/securite.svg"?>" class="rebric-icon"/>
                    <h5>Sécurité</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
                <div class="rebric-card justice-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/justice.svg"?>" class="rebric-icon"/>
                    <h5>Justice</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
                <div class="rebric-card education-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/education.svg"?>" class="rebric-icon"/>
                    <h5>Education</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
                <div class="rebric-card finance-border" >
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/finance.svg"?>" class="rebric-icon"/>
                    <h5>Finance et Budget</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
                <div class="rebric-card decentralisation-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/decentralisation.svg"?>" class="rebric-icon"/>
                    <h5>Décentralisation</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
            </div>
            <div class="rebrics-container mt-5 pb-5 ">
                <div class="rebric-card emploi-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/emploi.svg"?>" class="rebric-icon"/>
                    <h5>Emploi</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
                <div class="rebric-card culture-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/culture.svg"?>" class="rebric-icon"/>
                    <h5>Culture</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
                <div class="rebric-card sante-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/sante.svg"?>" class="rebric-icon"/>
                    <h5>Santé</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>
                <div class="rebric-card famille-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/famille.svg"?>" class="rebric-icon"/>
                    <h5>Affaires de la famille</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
                </div>

            </div>

        </div>
    </section>



<?php get_footer(); ?>