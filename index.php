<?php get_header() ?>
<section class="header" style="background-image: url( <?= get_template_directory_uri() . '/imgs/backgrounds/back-header-home.png' ?>);">

    <nav class="main-nav row">
        <div class="col-3">
            <div class="logo-container">
                <img src=<?= get_template_directory_uri() . "/imgs/logo/logo-blanc.png" ?> class="main-logo"/>
            </div>
        </div>
        <div class="col-6">
            <ul class="home-main-menu">
                <li><a href="<?=  site_url()?>">Accueil</a></li>
                <li><a href="<?=  site_url()?>/a-propos">A propos</a></li>
                <li><a href="<?=  site_url()?>/rubriques/">Rubriques</a></li>
                <li><a href="<?=  site_url()?>/experts/">Experts</a></li>
            </ul>
        </div>
        <div class="col-3 row">
            <div class="col-9 social-menu px-0">
                <ul>
                    <li><a href="#" onclick="toggleMenu()"><img src= <?= get_template_directory_uri() . "/imgs/icons/search.svg" ?> class="search-icon" /></a></li>
                    <li><span class="split-icon"></span></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/youtube.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook.svg"?> class="facebook-icon" /></a></li>
                </ul>
            </div>
            <div class="col-3 language-menu">
                <ul>
                    <li class="active"><a href="#"/>Fr </a></li>
                    <li>               <a href="#"/>Eng</a></li>
                    <li class="ar">    <a href="#"/>ع</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="home-header offset-md-1 col-md-7">

        <div class="header-title pt-5 ">
            <h1 class="white">La base de connaissances sur les
                politiques publiques</h1>

            <p class="px-0 white">
                <strong class="brown">HOULOUL</strong> est un outil en ligne rapide et pratique qui permet d’avoir accès à une large panoplie d’idées, de réflexions et de recommandations sur un sujet en
                particulier. En l’occurrence, notre plateforme traite de <strong>la Gouvernance en Tunisie</strong>.
            </p>
            <div class=" mx-0 px-0" style="width: 95%">
            <p class="see-more-1"> Voir Plus</p>
            <hr style="border-color: #9b8159;">
            </div>
        </div>

    </div>
    <div class="w-100 text-center pt-4" >

            <img src="<?= get_template_directory_uri() . "/imgs/icons/down.svg" ?>" class="go-down" />

    </div>
</section>
    <div class="rebric-theme" style="background-color: #9b8159"></div>

<section>
    <div class="search-screen"  id="home-menu">
        <div class="form-container">
            <img src="<?=  get_template_directory_uri(). "/imgs/icons/cancel.svg" ?>"  onclick="toggleMenu()" class="cancel-menu" />
            <h4 class="blue fs-big text-center semi-bold">Recherche</h4>
            <div class="search-section offset-2 col-md-8 mt-5">
                <div class="row search-form">
                    <div class="col-9">
                        <input type="text" class="search-input" placeholder="Que Cherchez-vous" />
                    </div>
                    <div class="col-3">
                        <button type="submit"class="search-btn">Chercher</button>
                    </div>
                </div>

                <div class="top-searchs">
                    <p>Recherches populaires</p>
                    <div class="thematique-subsection">
                        <ul>
                            <li>Justice</li>
                            <li><span class="separator"></span></li>
                            <li>Emploi</li>
                            <li><span class="separator"></span></li>
                            <li>Tunisie</li>
                            <li><span class="separator"></span></li>
                            <li>Justice</li>
                            <li><span class="separator"></span></li>
                            <li>Emploi</li>
                            <li><span class="separator"></span></li>
                            <li>Tunisie</li>
                        </ul>
                        <br>
                        <ul>
                            <li>Loi et ligislation</li>
                            <li> <span class="separator"></span></li>
                            <li>Assemblée</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="w-100 text-center title-2 blue mt-5">Nos rubriques</div>
    <div class="container-fluid">
        <div class="rebrics-container   ">
            <div class="rebric-card securite-border">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/securite.svg"?>" class="rebric-icon"/>
                    <h5>Sécurité</h5>
                    <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
            <div class="rebric-card justice-border">
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/justice.svg"?>" class="rebric-icon"/>
                <h5>Justice</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
            <div class="rebric-card education-border">
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/education.svg"?>" class="rebric-icon"/>
                <h5>Education</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
            <div class="rebric-card finance-border" >
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/finance.svg"?>" class="rebric-icon"/>
                <h5>Finance et Budget</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
            <div class="rebric-card decentralisation-border">
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/decentralisation.svg"?>" class="rebric-icon"/>
                <h5>Décentralisation</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
        </div>
        <div class="rebrics-container mt-5  ">
            <div class="rebric-card emploi-border">
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/emploi.svg"?>" class="rebric-icon"/>
                <h5>Emploi</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
            <div class="rebric-card culture-border">
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/culture.svg"?>" class="rebric-icon"/>
                <h5>Culture</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
            <div class="rebric-card sante-border">
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/sante.svg"?>" class="rebric-icon"/>
                <h5>Santé</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>
            <div class="rebric-card famille-border">
                <img src="<?= get_template_directory_uri() . "/imgs/icons/rubriques/famille.svg"?>" class="rebric-icon"/>
                <h5>Affaires de la famille</h5>
                <p>Lorem ipsum dolor sit amet, dolor sit </p>
            </div>

        </div>

    </div>
</section>

<section>
    <div class="policy-brief-section">
        <div class="w-100 text-center title-2 blue ">Dernièrs Policy Brief</div>

        <div class="policy-brief-container">
            <div class="container">

               <div class="policy-brief-card-big" style="border-bottom: 10px #ffd573 solid">
                    <div class="rubric">
                        <h4>Education</h4>
                        <p>Infrastructure scolaire</p>
                    </div>
                    <h4 class="title-2 fs-4 col-8 px-0">À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h4>
                    <p class="col-9 px-0">Le 16 mai 2019, Facebook annonce la désactivation de 265 pages ou comptes liés à une société israélienne dont le but est d’influencer l’opinion publique, principalement dans des pays africains. En Tunisie, 11 pages sont concernées.  </p>
                    <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                    <div class="tags">
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                    </div>

                </div>

                <div class="onerow-policy ">
                    <div class="policy-brief-card-m" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Alya et Bassem, 3400 DT par mois, gagner sa vie derrière un écran</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                        </div>
                    </div>

                    <div class="policy-brief-card-m" style="border-bottom: 10px #ffd573 solid">
                    <div class="rubric">
                        <h4>Education</h4>
                        <p>Infrastructure scolaire</p>
                    </div>
                    <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                    <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                    <div class="tags">
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                    </div>
                </div>
                </div>

                <div class="onerow-policy ">
                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Alya et Bassem, 3400 DT par mois, gagner sa vie derrière un écran</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>

                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>

                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="see-more-2 mt-5">
            <span>Afficher Plus</span>
            <img src="<?= get_template_directory_uri() . "/imgs/icons/down.svg" ?>" class="go-down" />
        </div>
    </div>


</section>


<section>
    <div class="w-100 text-center title-2 blue mt-5">Contact</div>

    <div class="contact-section container">
            <div class="row  mb-5" >
                <div class="col-3 text-center">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/phone.svg" ?>" />
                    <p>71 502 033 / 21 489 318</p>
                </div>
                <div class="col-3  text-center">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/envelope.svg" ?>" />
                    <p>Houloul@gmail.com</p>
                </div>
                <div class="col-3  text-center">
                    <img src="<?= get_template_directory_uri() . "/imgs/icons/placeholder.svg" ?>" />
                    <p>19 rue de sophie,<br>
                        Khaznadar, Bardo</p>
                </div>
            </div>
    </div>
</section>


<?php get_footer(); ?>