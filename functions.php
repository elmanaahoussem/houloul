<?php

/**
 * EnQueue Scripts
 */


function script_enqueue()
{
	wp_enqueue_style( 'MainStyle', get_template_directory_uri() . '/css/main.css', array(), '1.0.0', 'all' );
    wp_enqueue_script('popper', get_template_directory_uri() . '/js/popper.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'script_enqueue');




require_once __DIR__ . '/inc/gutenberg.php';
require_once __DIR__. '/inc/references.php';

function filter_images($content){
    return preg_replace('/<img (.*) \/>\s*/iU', '<div class="image-container-2"><div class="image-container"><img \1 /></div></div>', $content);
}
add_filter('the_content', 'filter_images');




/**
 * Theme Supports
 */
add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
add_theme_support('html5', array('search-form'));
add_theme_support('widgets');



