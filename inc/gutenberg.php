<?php

function guten_custom() {

    wp_register_script(
        'gutenberg-script',
        get_template_directory_uri().'/js/gutenberg-custom.js',
        array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' )
    );

    register_block_type('houloul/custom-cta',array(
        'editor_script' => 'gutenberg-script'
    ));

}
add_action( 'init', 'guten_custom' );