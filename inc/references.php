<?php 


class References {
	public static function init(){
		add_action('wp_enqueue_scripts', [self::class, 'loadFront']);
	}

	public static function loadFront(){
		if (is_single()){
			
		wp_enqueue_script( 'references-front', get_template_directory_uri().'/js/ref.front.js', ['jquery'] );
		}

	}

	public static function loadBackend(){

	}
}

add_action('wp', ['References','init']);