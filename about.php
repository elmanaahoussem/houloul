<?php
/*
    Template Name: About Page
*/
get_header();
the_post();
?>
<section class="header" style="background-image: url( <?= get_template_directory_uri() . '/imgs/backgrounds/back-header-home.png' ?>);">

    <nav class="main-nav row">
        <div class="col-3">
            <div class="logo-container">
                <img src=<?= get_template_directory_uri() . "/imgs/logo/logo-blanc.png" ?> class="main-logo"/>
            </div>
        </div>
        <div class="col-6">
            <ul class="home-main-menu">
                <li><a href="<?=  site_url()?>">Accueil</a></li>
                <li><a href="<?=  site_url()?>/a-propos">A propos</a></li>
                <li><a href="<?=  site_url()?>/rubriques/">Rubriques</a></li>
                <li><a href="<?=  site_url()?>/experts/">Experts</a></li>
            </ul>
        </div>
        <div class="col-3 row">
            <div class="col-9 social-menu px-0">
                <ul>
                    <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/search.svg" ?> class="search-icon" /></a></li>
                    <li><span class="split-icon"></span></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/youtube.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook.svg"?> class="facebook-icon" /></a></li>
                </ul>
            </div>
            <div class="col-3 language-menu">
                <ul>
                    <li class="active"><a href="#"/>Fr </a></li>
                    <li>               <a href="#"/>Eng</a></li>
                    <li class="ar">    <a href="#"/>ع</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="page-header  offset-md-2 col-md-8">
        <div class="header-title pt-5 ">
            <h1 class="white text-center fs-big"><?= get_the_title()?></h1>

        </div>
    </div>

    <div class="about-menu mt-5 pt-5" >
          <ul>
              <li><a>Présentation du projet</a></li>
              <li><span class="split-icon"></span></li>
              <li><a>Notre équipe</a></li>
              <li><span class="split-icon"></span></li>
              <li><a>Nos Partenaires</a></li>
          </ul>
    </div>

</section>


<div class="rebric-theme" style="background-color: #9b8159"></div>
<section>
    <div class="w-100 text-center title-2  mt-5">Présentation du projet</div>
    <div class="content-container px-0  pb-5 ">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius, odio urna hendrerit enim, a accumsan mi quam at mauris. Duis non dictum nibh, vitae ultricies odio. Praesent tempor metus vitae pharetra gravida. Aliquam luctus, metus eu consectetur euismod, lacus ligula fringilla tellus, at gravida ex lectus id nisl. Ut non tellus massa. Ut feugiat posuere massa ac tempor. Donec nec velit a elit hendrerit lacinia. </p>
        <h4 class="regular fs-5 mt-4">Objectif</h4>
        <p>Nam ut quam tortor. Aenean eu vehicula odio. Suspendisse vel pretium tellus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et augue magna. Nam non dolor dui. Donec volutpat diam ac sodales scelerisque. </p>
        <h4 class="regular fs-5 mt-4">Mission</h4>
        <p>Donec porttitor elementum lectus, a mollis tellus tristique id. Praesent et imperdiet dui. Sed ipsum elit, aliquam vitae euismod ut, vehicula a nisl. Pellentesque varius, tellus vitae iaculis dignissim, est massa tristique nunc</p>
    </div>

</section>

<section>
        <div class="section-name">
            <div class="white text-center fs-big semi-bold">Notre équipe</div>
        </div>
</section>

<section>
    <div class="content-container px-0">
        <h5 class="title-3 underline ">Les managers</h5>
        <div class="row mx-0 px-0 ">
            <div class="author-info col-6 pt-3 px-0">
                <h5>Monia Hamdi</h5>
                <h6>Chercheuse</h6>
                <p class="px-0 col-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>
                <div class="author-socials">
                    <ul>
                        <li class="email">unknown@email.com</li>
                        <li class="twitter">john@doe.com</li>
                        <li class="phone">21 480 480 / 50 335 666</li>
                    </ul>
                </div>
            </div>
            <div class="author-info col-6 pt-3 px-0">
                <h5>Monia Hamdi</h5>
                <h6>Chercheuse</h6>
                <p class="px-0 col-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>
                <div class="author-socials">
                    <ul>
                        <li class="email">unknown@email.com</li>
                        <li class="twitter">john@doe.com</li>
                        <li class="phone">21 480 480 / 50 335 666</li>
                    </ul>
                </div>
            </div>
            <div class="author-info col-6 pt-3 px-0">
                <h5>Monia Hamdi</h5>
                <h6>Chercheuse</h6>
                <p class="px-0 col-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>
                <div class="author-socials">
                    <ul>
                        <li class="email">unknown@email.com</li>
                        <li class="twitter">john@doe.com</li>
                        <li class="phone">21 480 480 / 50 335 666</li>
                    </ul>
                </div>
            </div>
            <div class="author-info col-6 pt-3 px-0">
                <h5>Monia Hamdi</h5>
                <h6>Chercheuse</h6>
                <p class="px-0 col-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>
                <div class="author-socials">
                    <ul>
                        <li class="email">unknown@email.com</li>
                        <li class="twitter">john@doe.com</li>
                        <li class="phone">21 480 480 / 50 335 666</li>
                    </ul>
                </div>
            </div>
        </div>

        <h5 class="title-3 underline mt-2 ">L'équipes</h5>

        <div class="team-members pb-2">
            <div class="team-member">
                <p> Hayfa Mzoughi <span> Infographiste</span> </p>
            </div>
            <div class="team-member">
                <p> Houssem ELManaa <span> Infographiste</span> </p>
            </div>
            <div class="team-member">
                <p> Hamdy Smika <span> Developpeur </span> </p>
            </div>
            <div class="team-member">
                <p> Moez ben chrifia <span> Infographiste</span> </p>
            </div>
            <div class="team-member">
                <p> Selim lAajimi <span> Infographiste</span> </p>
            </div>
        </div>

    </div>
</section>


<section>
    <div class="section-name">
        <div class="white text-center fs-big semi-bold">Nos partenaires</div>
    </div>

    <div class="content-container px-0  py-5 ">
        <div class="partners-container my-5">
            <div class="row text-center">
                <div class="col-4">
                     <img src="<?= get_template_directory_uri() . " /imgs/logo/logo1.png"  ?>"  />
                </div>
                <div class="col-4">
                    <img src="<?= get_template_directory_uri() . " /imgs/logo/logo2.png"  ?>"   style=" transform: translateY(25%);" />
                </div>
                <div class="col-4">
                    <img src="<?= get_template_directory_uri() . " /imgs/logo/logo3.png"  ?>"  style=" transform: translateY(-4%);"   />
                </div>
            </div>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius, odio urna hendrerit enim, a accumsan mi quam at mauris. Duis non dictum nibh, vitae ultricies odio. Praesent tempor metus vitae pharetra gravida. Aliquam luctus, metus eu consectetur euismod, lacus ligula fringilla tellus, at gravida ex lectus id nisl. Ut non tellus massa. Ut feugiat posuere massa ac tempor. Donec nec velit a elit hendrerit lacinia. </p>

        <p>Donec porttitor elementum lectus, a mollis tellus tristique id. Praesent et imperdiet dui. Sed ipsum elit, aliquam vitae euismod ut, vehicula a nisl. Pellentesque varius, tellus vitae iaculis dignissim, est massa tristique nunc</p>
    </div>


</section>


<?php get_footer(); ?>
