<?php
get_header();
the_post();
?>
    <section class="header" style="background-image: url( <?= get_template_directory_uri() . '/imgs/backgrounds/backheader.png' ?>);"  >

        <nav class="main-nav row">
            <div class="col-3">
                <div class="logo-container">
                    <img src=<?= get_template_directory_uri() . "/imgs/logo/logo.png" ?> class="main-logo"/>
                </div>
            </div>
            <div class="col-6">
                <ul class="main-menu">
                    <li><a href="<?=  site_url()?>">Accueil</a></li>
                    <li><a href="<?=  site_url()?>/a-propos">A propos</a></li>
                    <li><a href="<?=  site_url()?>/rubriques/">Rubriques</a></li>
                    <li><a href="<?=  site_url()?>/experts/">Experts</a></li>
                </ul>
            </div>
            <div class="col-3 row">
                <div class="col-9 social-menu px-0">
                    <ul>
                        <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/search.svg" ?> class="search-icon" /></a></li>
                        <li><span class="split-icon"></span></li>
                        <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/youtube.svg" ?> class="search-icon" /></a></li>
                        <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter.svg" ?> class="search-icon" /></a></li>
                        <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook.svg"?> class="facebook-icon" /></a></li>
                    </ul>
                </div>
                <div class="col-3 language-menu">
                    <ul>
                        <li class="active"><a href="#"/>Fr </a></li>
                        <li><a href="#"/>Eng</a></li>
                        <li class="ar"><a href="#"/>ع</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="page-header offset-md-2 col-md-8">
            <div class="rubric">
                <h4><?=  get_the_category()[0]->name ?></h4>
                <p><?=  get_the_category()[1]->name ?></p>
            </div>
            <div class="header-title">
                <h1>  <?= get_the_title() ?></h1>
                <p class="col-11 px-0">
                    <?= get_the_excerpt() ?>
                </p>
                <hr>
                <p class="author">Par <?php the_author_meta('display_name') ?> | <?= get_field("fonction",'user_'.get_the_author_meta('ID')) ?> </p>
                <p class="date pb-2"><?= get_the_date('j F Y' ) ?>  </p>
            </div>
        </div>

    </section>
    <div class="rebric-theme"></div>

    <section>
        <div class="col-12 row mx-0 intro-section">
            <div class="col-md-2">
                <div class="sharer-module">
                    <h6>Partager</h6>
                    <div class="socials-logos">
                        <ul>
                            <li><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter-black.svg" ?> class="social-icons"/></li>
                            <li><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook-black.svg"  ?> class="social-icons"/></li>
                            <li><img src=<?= get_template_directory_uri() . "/imgs/icons/envelope-black.svg" ?>  class="social-icons"/></li>
                        </ul>
                    </div>
                    <h6 class="mt-5 download-btn">Télecharger</h6>
                </div>
            </div>
            <div class="intro col-md-8">
                <p><?=  get_field('intro')?></p>
            </div>
        </div>
        <div class="content-container single-content-text " id="post-content">
            <?php the_content() ?>
        </div>
    </section>
    <section class="mb-5">
        <div class="tags-container content-container">
            <div class="tags ">
                <h6>Thèmes associés</h6>
                <?php foreach (get_the_tags() as $tag) :?>
                    <span>#<?= $tag->name ?></span>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section>
        <div class="ref-biblio-container content-container py-5 ">
            <div class="ref-biblio row mx-0">
                <div class="offset-1 col-10">
                    <h6>Références bibliographiques</h6>
                    <?php the_field("ref_biblio") ?>
                </div>

            </div>
        </div>
    </section>

    <section>
        <div class="authors-bg-1">
            <div class=" content-container">
                <h5 class="title-3">Les contributeurs</h5>
                <hr color="black">
                <div class="author-info">
                    <h5><?php the_author_meta('display_name') ?></h5>
                    <h6><?= get_field("fonction",'user_'.get_the_author_meta('ID')) ?> </h6>
                    <p class="px-0 col-8"><?php the_author_meta('description') ?></p>
                    <div class="author-socials">
                        <ul>
                            <li class="email"><?php the_author_meta('user_email') ?></li>
                            <li class="twitter"><?= get_field("twitter",'user_'.get_the_author_meta('ID')) ?></li>
                            <li class="phone"><?= get_field("telephone",'user_'.get_the_author_meta('ID')) ?></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="related-posts-container container">
            <h5 class="title-3">Sur le même sujet</h5>
            <hr color="black">
            <div class="row mx-0">
                <div class="related-post col-4">
                    <div class="rubric mb-4">
                        <h4>Sécurité</h4>
                        <p>Justice transitionnelle</p>
                    </div>
                    <h5>À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h5>
                </div>
                <div class="related-post col-4">
                    <div class="rubric mb-4">
                        <h4>Sécurité</h4>
                        <p>Justice transitionnelle</p>
                    </div>
                    <h5>À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h5>
                </div>
                <div class="related-post col-4">
                    <div class="rubric mb-4">
                        <h4>Sécurité</h4>
                        <p>Justice transitionnelle</p>
                    </div>
                    <h5>À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h5>
                </div>
            </div>
        </div>
    </section>

    <section>
        <a href="#pagebody">
        <div class="w-100 text-center backtotop mb-3">

                <p class="my-0">Back to top</p>
            <img src=<?= get_template_directory_uri() . "/imgs/icons/arrow.svg" ?> class="backtop-arrow"/>

        </div>
        </a>
    </section>

<?php get_footer(); ?>