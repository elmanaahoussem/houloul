$ = jQuery;

jQuery.fn.detectRef = function(){
	let html = $(this).html();
	let match = true;
	html =  html.replace(/(\[\d+\])/g, function(x){
		let d = x.replace('[','').replace(']', '');
		return '<sup data-ref="'+d+'" class="reference-link"><strong>'+d+'</strong></sup>';
	})
	$(this).html(html);
}



jQuery.fn.makeRef = function(){
	let refBlock = 	$(this).find('.block-references');
	if (refBlock.length !== 1) {
		return;
	}

	$(this).find('p').each(function(){
		$(this).detectRef();
	});
	function parseAfter(res, node){
		if (!node.nextSibling) return res;
		if (node.nextSibling.nodeType===1 && node.nextSibling.tagName.toLowerCase()==='sup') {
			return res;
		}
		if (node.nextSibling.nodeType===3) {
			res = res+node.nextSibling.textContent;
		}
		else{
			res = res+node.nextSibling.outerHTML;
		}
		return parseAfter(res, node.nextSibling);

	}
	let refTable = []
	refBlock.find('sup').each(function(){
		let nRef = $(this).data('ref');
		let textRef = parseAfter('',this);
		refTable.push({nRef, textRef, ref:this})
	});

	refTable.map(refInfo=>{
		let match = $(this).find('sup[data-ref="'+refInfo.nRef+'"]');
		match.each(function(){
			if ($(this).parents('.block-references').length) return;
			let oldrefs = $(this).parents('p').data('refs');
			if (!oldrefs) oldrefs = [];
			oldrefs.push(refInfo.nRef);
			$(this).parents('p').data('refs', oldrefs).addClass('has-ref');
		});
	});

	function updatePosition(){
		$('.has-ref').each(function(){
			let bbox = this.getBoundingClientRect();
			$(this).next().css({
				top: bbox.top+'px',
				width: '177px',
			//	left: (bbox.left - 215)+'px'
                left: '1.5%'
			});
		})
	}
	//construction
	$('.has-ref', this).each(function(){
		let refs = $(this).data('refs'); // [1,2,4]
		$(this).after('<div style="position:fixed;" class="small ref-juridique"><h6 class="mb-4">Référence juridique</h6></div>');
		refs.map(n=>{
			refTable.filter(x=>x.nRef===n).map(x=>{
				$(this).next().append('<div class="mb-2 ref"><p class="ref-number">'+x.nRef+'<span class="ref-pipe"></span></p> <p>'+x.textRef+'</p></div>')
			});
		});
	});
	$(window).scroll(updatePosition);
}


jQuery(document).ready(function(){
	jQuery('#post-content').each(function(){
		jQuery(this).makeRef();
	})
});