<?php
/*
    Template Name: Search Result Template
*/
get_header();
the_post();
?>
<section class="header not-full-screen" style="background-image: url( <?= get_template_directory_uri() . '/imgs/backgrounds/back-gris.png' ?>);">
    <nav class="main-nav row">
        <div class="col-3">
            <div class="logo-container">
                <img src=<?= get_template_directory_uri() . "/imgs/logo/logo.png" ?> class="main-logo"/>
            </div>
        </div>
        <div class="col-6">
            <ul class="main-menu">
                <li><a href="<?=  site_url()?>">Accueil</a></li>
                <li><a href="<?=  site_url()?>/a-propos">A propos</a></li>
                <li><a href="<?=  site_url()?>/rubriques/">Rubriques</a></li>
                <li><a href="<?=  site_url()?>/experts/">Experts</a></li>
            </ul>
        </div>
        <div class="col-3 row">
            <div class="col-9 social-menu px-0">
                <ul>
                    <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/search.svg" ?> class="search-icon" /></a></li>
                    <li><span class="split-icon"></span></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/youtube.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook.svg"?> class="facebook-icon" /></a></li>
                </ul>
            </div>
            <div class="col-3 language-menu">
                <ul>
                    <li class="active"><a href="#"/>Fr </a></li>
                    <li>               <a href="#"/>Eng</a></li>
                    <li class="ar">    <a href="#"/>ع</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="offset-md-2 col-md-8">
       <div class="search-section">
           <p>20 Resultats pour :</p>
           <div class="row search-form py-5">
               <div class="col-9">
                <input type="text" class="search-input" placeholder="Que Cherchez-vous" />
               </div>
               <div class="col-3">
                 <button type="submit"class="search-btn">Chercher</button>
               </div>
           </div>
       </div>
    </div>

    <div class="rebric-theme" style="background-color: #083d72 ;height: 11px ">
</section>



<section>

    <div class="policy-brief-section mt-0">

        <div class="policy-brief-container ">
            <div class="container">
                <div class="search-filters">
                    <p>Filtrer par: </p>
                    <select>
                        <option>Rubriques</option>
                    </select>
                    <span class="separator"></span>
                    <select>
                        <option>Thémes</option>
                    </select>
                    <span class="separator"></span>
                    <select>
                        <option>Date</option>
                    </select>
                </div>

                <div class="policy-brief-card-big mb-5" style="border-bottom: 10px #ffd573 solid">
                    <div class="rubric">
                        <h4>Education</h4>
                        <p>Infrastructure scolaire</p>
                    </div>
                    <h4 class="title-2 fs-4 col-8 px-0">À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h4>
                    <p class="col-9 px-0">Le 16 mai 2019, Facebook annonce la désactivation de 265 pages ou comptes liés à une société israélienne dont le but est d’influencer l’opinion publique, principalement dans des pays africains. En Tunisie, 11 pages sont concernées.  </p>
                    <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                    <div class="tags">
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                    </div>

                </div>


                <div class="policy-brief-card-big mb-5" style="border-bottom: 10px #ffd573 solid">
                    <div class="rubric">
                        <h4>Education</h4>
                        <p>Infrastructure scolaire</p>
                    </div>
                    <h4 class="title-2 fs-4 col-8 px-0">À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h4>
                    <p class="col-9 px-0">Le 16 mai 2019, Facebook annonce la désactivation de 265 pages ou comptes liés à une société israélienne dont le but est d’influencer l’opinion publique, principalement dans des pays africains. En Tunisie, 11 pages sont concernées.  </p>
                    <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                    <div class="tags">
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                    </div>

                </div>


                <div class="policy-brief-card-big mb-5" style="border-bottom: 10px #ffd573 solid">
                    <div class="rubric">
                        <h4>Education</h4>
                        <p>Infrastructure scolaire</p>
                    </div>
                    <h4 class="title-2 fs-4 col-8 px-0">À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h4>
                    <p class="col-9 px-0">Le 16 mai 2019, Facebook annonce la désactivation de 265 pages ou comptes liés à une société israélienne dont le but est d’influencer l’opinion publique, principalement dans des pays africains. En Tunisie, 11 pages sont concernées.  </p>
                    <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                    <div class="tags">
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                    </div>

                </div>


            </div>
        </div>


        <div class="see-more-2 mt-5">
            <span>Afficher Plus</span>
            <img src="<?= get_template_directory_uri() . "/imgs/icons/down.svg" ?>" class="go-down" />
        </div>

    </div>
</section>
<?php get_footer(); ?>
