<?php
/*
    Template Name: thematique Page
*/
get_header();
the_post();
?>
<section class="header not-full-screen" style="background-image: url( <?= get_template_directory_uri() . '/imgs/backgrounds/back-gris.png' ?>);">
    <nav class="main-nav row">
        <div class="col-3">
            <div class="logo-container">
                <img src=<?= get_template_directory_uri() . "/imgs/logo/logo.png" ?> class="main-logo"/>
            </div>
        </div>
        <div class="col-6">
            <ul class="main-menu">
                <li><a href="/houloul">Accueil</a></li>
                <li><a href="/houloul/a-propos">A propos</a></li>
                <li><a href="/houloul/rubriques">Rubriques</a></li>
                <li><a href="#">Experts</a></li>
            </ul>
        </div>
        <div class="col-3 row">
            <div class="col-9 social-menu px-0">
                <ul>
                    <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/search.svg" ?> class="search-icon" /></a></li>
                    <li><span class="split-icon"></span></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/youtube.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook.svg"?> class="facebook-icon" /></a></li>
                </ul>
            </div>
            <div class="col-3 language-menu">
                <ul>
                    <li class="active"><a href="#"/>Fr </a></li>
                    <li>               <a href="#"/>Eng</a></li>
                    <li class="ar">    <a href="#"/>ع</a></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="home-header offset-md-2 col-md-8">
        <div class="header-title pt-2 ">
            <h1 class=" text-center fs-1 mb-4">Justice transitionnelle</h1>
            <div class=" col-md-10 offset-md-1 " >
                <hr style="border-color: #9b8159;">
            </div>
        </div>
        <div class="thematique-subsection offset-1 px-5 pb-4 pt-3">
            <p>Rubrique : </p> <span class="rubric-icon"> Justice </span> <br>
            <p>Thémes associés : </p>
            <ul>
                <li>Accès à la justice</li>
                <li><span class="separator"></span></li>
                <li>Réforme des institutions de la Justice</li>
            </ul>

        </div>
    </div>
    <div class="rebric-theme" style="background-color: #083d72 ;height: 11px ">
</section>


<section>
    <div class="policy-brief-section mt-0 ">
        <div class="policy-brief-container pt-5">
            <div class="container">

                <div class="policy-brief-card-big" style="border-bottom: 10px #ffd573 solid">
                    <div class="rubric">
                        <h4>Education</h4>
                        <p>Infrastructure scolaire</p>
                    </div>
                    <h4 class="title-2 fs-4 col-8 px-0">À qui profite le contenu des pages facebook tunisiennes liées à Israël ?</h4>
                    <p class="col-9 px-0">Le 16 mai 2019, Facebook annonce la désactivation de 265 pages ou comptes liés à une société israélienne dont le but est d’influencer l’opinion publique, principalement dans des pays africains. En Tunisie, 11 pages sont concernées.  </p>
                    <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                    <div class="tags">
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                        <span>#justice </span>
                    </div>

                </div>

                <div class="onerow-policy ">
                    <div class="policy-brief-card-m" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Alya et Bassem, 3400 DT par mois, gagner sa vie derrière un écran</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                        </div>
                    </div>

                    <div class="policy-brief-card-m" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                        </div>
                    </div>
                </div>

                <div class="onerow-policy ">
                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Alya et Bassem, 3400 DT par mois, gagner sa vie derrière un écran</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>

                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>

                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="experts-section ">
            <div class="content-container px-0">
            <h5 class="title-3 underline white mt-0 col-11 px-0">Les experts qui ont contribué à ce théme</h5>
            <div class="row mx-0 px-0 ">
                <div class="author-info col-6 px-0">
                    <h5 class="white">Monia Hamdi</h5>
                    <h6>Chercheuse</h6>
                    <p class="px-0 col-11 white"> Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>

                </div>
                <div class="author-info col-6 px-0">
                    <h5 class="white">Monia Hamdi</h5>
                    <h6>Chercheuse</h6>
                    <p class="px-0 col-11 white"> Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>

                </div>
                <div class="author-info col-6 px-0">
                    <h5 class="white">Monia Hamdi</h5>
                    <h6>Chercheuse</h6>
                    <p class="px-0 col-11 white"> Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>

                </div>
                <div class="author-info col-6 px-0">
                    <h5 class="white" >Monia Hamdi</h5>
                    <h6>Chercheuse</h6>
                    <p class="px-0 col-11 white"> Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>

                </div>
            </div>

        </div>
        </div>

        <div class="policy-brief-container pt-5">
            <div class="container">



                <div class="onerow-policy ">
                    <div class="policy-brief-card-m" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Alya et Bassem, 3400 DT par mois, gagner sa vie derrière un écran</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                        </div>
                    </div>

                    <div class="policy-brief-card-m" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>
                        </div>
                    </div>
                </div>

                <div class="onerow-policy ">
                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Alya et Bassem, 3400 DT par mois, gagner sa vie derrière un écran</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>

                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>

                    <div class="policy-brief-card-s" style="border-bottom: 10px #ffd573 solid">
                        <div class="rubric">
                            <h4>Education</h4>
                            <p>Infrastructure scolaire</p>
                        </div>
                        <h4 class="title-2 fs-6 col-10 px-0">Violences sexuelles en Ukraine, le long chemin vers la justice</h4>
                        <p class="author">Par Monia Hamdi | 03 Juin 2019  </p>
                        <div class="tags">
                            <span>#justice </span>
                            <span>#justice </span>
                            <span>#justice </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="see-more-2 mt-5">
            <span>Afficher Plus</span>
            <img src="<?= get_template_directory_uri() . "/imgs/icons/down.svg" ?>" class="go-down" />
        </div>
    </div>

</section>

<?php get_footer(); ?>
