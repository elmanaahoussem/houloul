<footer>
    <div class="footer" style="height: 252px">
        <div class="main-nav row">
            <div class="col-3">
                <div class="logo-container">
                    <img src=<?= get_template_directory_uri() . "/imgs/logo/logo-blanc.png" ?> class="main-logo"/>
                </div>
            </div>
            <div class="col-6 menu-container">
                <ul class="main-menu">
                    <li><a href="<?=  site_url()?>">Accueil</a></li>
                    <li><a href="<?=  site_url()?>/a-propos">A propos</a></li>
                    <li><a href="<?=  site_url()?>/rubriques/">Rubriques</a></li>
                    <li><a href="<?=  site_url()?>/experts/">Experts</a></li>
                </ul>
            </div>
            <div class="col-3 row">
                <div class=" col-md-12 social-menu px-0">
                    <ul>
                        <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/youtube-light.svg" ?>  class="search-icon"/></a></li>
                        <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/twitter-light.svg" ?>  class="search-icon"/></a></li>
                        <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/facebook-light.svg" ?> class="facebook-icon"/></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
