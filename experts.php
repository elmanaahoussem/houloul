<?php
/*
  Template Name: Experts Page
*/
get_header();
the_post();
?>

<section class="header" style="background-image: url( <?= get_template_directory_uri() . '/imgs/backgrounds/back-header-home.png' ?>);">
    <nav class="main-nav row">
        <div class="col-3">
            <div class="logo-container">
                <img src=<?= get_template_directory_uri() . "/imgs/logo/logo-blanc.png" ?> class="main-logo"/>
            </div>
        </div>
        <div class="col-6">
            <ul class="home-main-menu">
                <li><a href="<?=  site_url()?>">Accueil</a></li>
                <li><a href="<?=  site_url()?>/a-propos">A propos</a></li>
                <li><a href="<?=  site_url()?>/rubriques/">Rubriques</a></li>
                <li><a href="<?=  site_url()?>/experts/">Experts</a></li>
            </ul>
        </div>
        <div class="col-3 row">
            <div class="col-9 social-menu px-0">
                <ul>
                    <li><a><img src= <?= get_template_directory_uri() . "/imgs/icons/search.svg" ?> class="search-icon" /></a></li>
                    <li><span class="split-icon"></span></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/youtube.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/twitter.svg" ?> class="search-icon" /></a></li>
                    <li><a><img src=<?= get_template_directory_uri() . "/imgs/icons/facebook.svg"?> class="facebook-icon" /></a></li>
                </ul>
            </div>
            <div class="col-3 language-menu">
                <ul>
                    <li class="active"><a href="#"/>Fr </a></li>
                    <li>               <a href="#"/>Eng</a></li>
                    <li class="ar">    <a href="#"/>ع</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="home-header offset-md-2 col-md-8">
        <div class="header-title pt-2 ">
            <h1 class="white text-center fs-big"><?= get_the_title()?></h1>
            <div class="px-0 p-white pt-5">
              <?= get_the_content() ?>
            </div>
            <div class=" mx-0 px-0" >
                <hr style="border-color: #9b8159;">
            </div>
        </div>
    </div>
</section>

<div class="rebric-theme" style="background-color: #9b8159"></div>

<section>
    <div class="py-5 bg-grey experts-container">
        <div class="aphabetic-filter offset-md-2 col-md-8">
            <ul>
            <?php foreach (range('A', 'Z') as $char) :?>
            <li><?= $char ?></li>
            <?php endforeach; ?>
            </ul>
        </div>

        <div class="offset-md-1 col-md-10 mt-5">
            <div class="row d-flex justify-content-between">
                <div class="author-card  ">
                    <div class="author-info  px-0">
                        <h5>Monia Hamdi</h5>
                        <h6>Chercheuse</h6>
                        <p  >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>
                        <div class="author-socials">
                            <ul>
                                <li class="email">unknown@email.com</li>
                                <li class="twitter">john@doe.com</li>
                                <li class="phone">21 480 480 / 50 335 666</li>
                            </ul>
                        </div>
                        <div class="w-100 text-right profile-arrow">
                            <img src="<?= get_template_directory_uri() . "/imgs/icons/fleche-expert.svg" ?>">
                        </div>
                    </div>
                </div>
                <div class="author-card  ">
                    <div class="author-info  px-0">
                        <h5>Monia Hamdi</h5>
                        <h6>Chercheuse</h6>
                        <p  >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>
                        <div class="author-socials">
                            <ul>
                                <li class="email">unknown@email.com</li>
                                <li class="twitter">john@doe.com</li>
                                <li class="phone">21 480 480 / 50 335 666</li>
                            </ul>
                        </div>
                        <div class="w-100 text-right profile-arrow">
                            <img src="<?= get_template_directory_uri() . "/imgs/icons/fleche-expert.svg" ?>">
                        </div>
                    </div>
                </div>
                <div class="author-card ">
                    <div class="author-info  px-0">
                        <h5>Monia Hamdi</h5>
                        <h6>Chercheuse</h6>
                        <p  >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae pharetra velit. Donec eu lectus gravida, bibendum urna eget, varius augue. Suspendisse interdum, neque non auctor varius</p>
                        <div class="author-socials">
                            <ul>
                                <li class="email">unknown@email.com</li>
                                <li class="twitter">john@doe.com</li>
                                <li class="phone">21 480 480 / 50 335 666</li>
                            </ul>
                        </div>
                        <div class="w-100 text-right profile-arrow">
                                <img src="<?= get_template_directory_uri() . "/imgs/icons/fleche-expert.svg" ?>">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="see-more-2 mt-5">
            <span>Afficher Plus</span>
            <img src="<?= get_template_directory_uri() . "/imgs/icons/down.svg" ?>" class="go-down" />
        </div>
    </div>
</section>



<?php get_footer(); ?>
